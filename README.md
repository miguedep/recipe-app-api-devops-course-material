# DevOps deployment automation with Terraform, AWS and Docker - Supplementary Material

Supplementary material for our course: [DevOps deployment automation with Terraform, AWS and Docker](https://londonappdeveloper.thinkific.com/courses/devops-deployment-automation-terraform-aws-docker)

# Contents

This document contains the following:

 * [commands-reference.md](./commands-reference.md) - A reference sheet with the common commands used in this course.
 * [Makefile](./Makefile) - Sample `Makefile` which could be added to your project to reduce the length of the commands you need to run.
 * [windows-notes.md](./windows-notes.md) - Notes and tips for running the steps on Windows machines.
